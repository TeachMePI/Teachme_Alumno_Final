package pe.com.teachme.teachme.Fragments;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.ArrayList;

import pe.com.teachme.teachme.Adapter.ProfesorAdapter;
import pe.com.teachme.teachme.AnuncioActivity;
import pe.com.teachme.teachme.Entity.ProfesorEntity;
import pe.com.teachme.teachme.R;

public class FrgProfesor extends Fragment {
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private RecyclerView recyclerView;
    private ProfesorAdapter anuncioAdapter;
    private ArrayList<ProfesorEntity> anuncioEntities;
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public FrgProfesor() {
        // Required empty public constructor
    }

    public static FrgProfesor newInstance(String param1, String param2) {
        FrgProfesor fragment = new FrgProfesor();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_frg_anuncios, container, false);
        recyclerView = (RecyclerView)view.findViewById(R.id.lstAnuncios);
        recyclerView.setHasFixedSize(true);
        int color = Color.parseColor("#FFFFFF");
        FloatingActionButton floatingActionButton = (FloatingActionButton)view.findViewById(R.id.fab);
        floatingActionButton.setColorFilter(color);
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), AnuncioActivity.class);
                //intent.putExtra("id", id);
                startActivity(intent);
            }
        });
        RecyclerView.LayoutManager ly = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(ly);
        anuncioEntities = new ArrayList<>();
        anuncioEntities.add(new ProfesorEntity("1", "JORGE","SAAVEDRA MEDINA", "HOMBRE", "Matematica","50 soles / hora"));
        anuncioEntities.add(new ProfesorEntity("2", "ALEJANDRO","CORNEJO PIÑERA", "HOMBRE", "Programacion","30 soles / hora"));
        anuncioEntities.add(new ProfesorEntity("3", "JULIAN","TORRES ADELÑO", "HOMBRE", "Fisica","70 soles / hora"));
        anuncioEntities.add(new ProfesorEntity("4", "MARIANA","CASTILLO LECA", "MUJER", "Psicolia","10 soles / hora"));
        anuncioEntities.add(new ProfesorEntity("5", "INGRID","GUZMAN RODRIGUEZ", "MUJER", "Literatura","40 soles / hora"));

        anuncioAdapter = new ProfesorAdapter(anuncioEntities);
        anuncioAdapter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String titulo = anuncioEntities.get(recyclerView.getChildAdapterPosition(v)).getName();
                Toast.makeText(getActivity(),titulo, Toast.LENGTH_SHORT).show();
            }
        });
        recyclerView.setAdapter(anuncioAdapter);
        return view;
    }

    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
