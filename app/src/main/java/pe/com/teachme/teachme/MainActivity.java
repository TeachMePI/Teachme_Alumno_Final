package pe.com.teachme.teachme;

import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import pe.com.teachme.teachme.Fragments.FrgAnuncios;
import pe.com.teachme.teachme.Fragments.FrgChat;
import pe.com.teachme.teachme.Fragments.FrgEstadistica;
import pe.com.teachme.teachme.Fragments.FrgHistorial;
import pe.com.teachme.teachme.Fragments.FrgHorario;
import pe.com.teachme.teachme.Fragments.FrgPerfil;
import pe.com.teachme.teachme.Fragments.FrgProfesor;
import pe.com.teachme.teachme.Fragments.FrgPuntos;
import pe.com.teachme.teachme.Fragments.FrgRecomendacion;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener,
        FrgAnuncios.OnFragmentInteractionListener,
        FrgChat.OnFragmentInteractionListener,
        FrgEstadistica.OnFragmentInteractionListener,
        FrgHorario.OnFragmentInteractionListener,
        FrgPerfil.OnFragmentInteractionListener,
        FrgPuntos.OnFragmentInteractionListener,
        FrgRecomendacion.OnFragmentInteractionListener,
        FrgProfesor.OnFragmentInteractionListener,
        FrgHistorial.OnFragmentInteractionListener{


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

       /* FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });*/

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        FrgPuntos fragment = new FrgPuntos();
        getSupportActionBar().setTitle("Puntos");
        getSupportFragmentManager().beginTransaction().replace(R.id.contenedorFragment, fragment).commit();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        Fragment fragment =null;
        boolean frg = false;
        if (id == R.id.itemperfil) {
            fragment = new FrgPerfil();
            getSupportActionBar().setTitle("Perfil");
            frg=true;
        } else if (id == R.id.itemhistorial) {
            fragment = new FrgHistorial();
            getSupportActionBar().setTitle("Historial");
            frg=true;
        } else if (id == R.id.itembuscar) {
            fragment = new FrgProfesor();
            getSupportActionBar().setTitle("Profesor");
            frg=true;
        } else if (id == R.id.itemChat) {
            fragment = new FrgChat();
            getSupportActionBar().setTitle("Chat");
            frg=true;
        }else if (id == R.id.itemPuntos) {
            fragment = new FrgPuntos();
            getSupportActionBar().setTitle("Calificaciones");
            frg=true;
        } else if (id == R.id.itemSalir) {

        }
        if(frg){
            getSupportFragmentManager().beginTransaction().replace(R.id.contenedorFragment, fragment).commit();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }
}

