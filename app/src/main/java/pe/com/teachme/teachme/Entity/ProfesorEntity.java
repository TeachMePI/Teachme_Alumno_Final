package pe.com.teachme.teachme.Entity;

public class ProfesorEntity {
    String id;
    String name;
    String lastname;
    String sex;
    String curso;
    String precio;

    public ProfesorEntity(String id, String name, String lastname, String sex, String curso, String precio) {
        this.id = id;
        this.name = name;
        this.lastname = lastname;
        this.sex = sex;
        this.curso = curso;
        this.precio = precio;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getCurso() {
        return curso;
    }

    public void setCurso(String curso) {
        this.curso = curso;
    }

    public String getPrecio() {
        return precio;
    }

    public void setPrecio(String precio) {
        this.precio = precio;
    }
}

