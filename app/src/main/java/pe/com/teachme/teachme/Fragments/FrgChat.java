package pe.com.teachme.teachme.Fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.ArrayList;

import pe.com.teachme.teachme.Adapter.ChatAdapter;
import pe.com.teachme.teachme.Entity.ChatEntity;
import pe.com.teachme.teachme.R;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link FrgChat.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link FrgChat#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FrgChat extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public FrgChat() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FrgChat.
     */
    // TODO: Rename and change types and number of parameters
    public static FrgChat newInstance(String param1, String param2) {
        FrgChat fragment = new FrgChat();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    RecyclerView recyclerView;
    ChatAdapter chatAdapter;
    ArrayList<ChatEntity> chatEntities;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_frg_chat, container, false);
        recyclerView = (RecyclerView)view.findViewById(R.id.lstChats);
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager ly = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(ly);
        chatEntities = new ArrayList<>();
        chatEntities.add(new ChatEntity(null,"Hola, me desaprobaron", "12:05", "Marta",false));
        chatEntities.add(new ChatEntity(null,"te veo en Titos", "13:05", "Jimmy",true));
        chatEntities.add(new ChatEntity(null,"como te llamas?", "16:05", "Cecilia",true));
        chatEntities.add(new ChatEntity(null,"ayudame con la tarea", "18:05", "Mirta",false));
        chatEntities.add(new ChatEntity(null,"mañana gana juega Perú!!!", "22:05", "Kevin",true));
        chatAdapter = new ChatAdapter(chatEntities);
        chatAdapter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String titulo = chatEntities.get(recyclerView.getChildAdapterPosition(v)).getPersona();
                Toast.makeText(getActivity(),titulo, Toast.LENGTH_SHORT).show();
            }
        });
        recyclerView.setAdapter(chatAdapter);
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
