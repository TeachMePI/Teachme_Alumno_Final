package pe.com.teachme.teachme.Fragments;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.ArrayList;

import pe.com.teachme.teachme.Adapter.HistorialAdapter;
import pe.com.teachme.teachme.AnuncioActivity;
import pe.com.teachme.teachme.Entity.HistorialEntity;
import pe.com.teachme.teachme.R;

public class FrgHistorial extends Fragment {
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private RecyclerView recyclerView;
    private HistorialAdapter anuncioAdapter;
    private ArrayList<HistorialEntity> anuncioEntities;
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public FrgHistorial() {
        // Required empty public constructor
    }

    public static FrgHistorial newInstance(String param1, String param2) {
        FrgHistorial fragment = new FrgHistorial();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_frg_historial, container, false);
        recyclerView = (RecyclerView)view.findViewById(R.id.lstAnuncios);
        recyclerView.setHasFixedSize(true);
        int color = Color.parseColor("#FFFFFF");
        FloatingActionButton floatingActionButton = (FloatingActionButton)view.findViewById(R.id.fab);
        floatingActionButton.setColorFilter(color);
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), AnuncioActivity.class);
                //intent.putExtra("id", id);
                startActivity(intent);
            }
        });
        RecyclerView.LayoutManager ly = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(ly);
        anuncioEntities = new ArrayList<>();
        anuncioEntities.add(new HistorialEntity("1", "Programacion","18/05/2018", "18/06/2018", "Nota: 10"));
        anuncioEntities.add(new HistorialEntity("2", "JAVA","11/04/2018", "14/04/2018", "Nota: 13"));
        anuncioEntities.add(new HistorialEntity("3", "Matematica","24/05/2018", "24/06/2018", "Nota: 17"));
        anuncioEntities.add(new HistorialEntity("4", "Fisica","04/05/2018", "04/06/2018", "Nota: 11"));
        anuncioEntities.add(new HistorialEntity("5", "Quimica","01/02/2018", "02/03/2018", "Nota: 07"));

        anuncioAdapter = new HistorialAdapter(anuncioEntities);
        anuncioAdapter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String titulo = anuncioEntities.get(recyclerView.getChildAdapterPosition(v)).getCurso();
                Toast.makeText(getActivity(),titulo, Toast.LENGTH_SHORT).show();
            }
        });
        recyclerView.setAdapter(anuncioAdapter);
        return view;
    }

    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
