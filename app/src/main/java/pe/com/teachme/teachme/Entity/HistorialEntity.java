package pe.com.teachme.teachme.Entity;

public class HistorialEntity {
    String id;
    String curso;
    String fechainicio;
    String fechafin;
    String nota;

    public HistorialEntity(String id, String curso, String fechainicio, String fechafin, String nota) {
        this.id = id;
        this.curso = curso;
        this.fechainicio = fechainicio;
        this.fechafin = fechafin;
        this.nota = nota;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCurso() {
        return curso;
    }

    public void setCurso(String curso) {
        this.curso = curso;
    }

    public String getFechainicio() {
        return fechainicio;
    }

    public void setFechainicio(String fechainicio) {
        this.fechainicio = fechainicio;
    }

    public String getFechafin() {
        return fechafin;
    }

    public void setFechafin(String fechafin) {
        this.fechafin = fechafin;
    }

    public String getNota() {
        return nota;
    }

    public void setNota(String nota) {
        this.nota = nota;
    }
}
