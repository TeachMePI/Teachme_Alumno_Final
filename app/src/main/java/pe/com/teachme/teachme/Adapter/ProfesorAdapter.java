package pe.com.teachme.teachme.Adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import pe.com.teachme.teachme.Entity.ProfesorEntity;
import pe.com.teachme.teachme.R;

public class ProfesorAdapter extends RecyclerView.Adapter<ProfesorAdapter.ViewHolder> implements View.OnClickListener {
    public List<ProfesorEntity> anuncioEntities;
    private View.OnClickListener listener;

    public ProfesorAdapter(List<ProfesorEntity> anuncioEntities) {
        this.anuncioEntities = anuncioEntities;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list_anuncio, parent,false);
        view.setOnClickListener(this);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ProfesorAdapter.ViewHolder holder, int position) {
        holder.txtAnuncioTitulo.setText(anuncioEntities.get(position).getName());
        holder.txtAnuncioLugar.setText(anuncioEntities.get(position).getLastname());
        holder.txtAnuncioMateria.setText(anuncioEntities.get(position).getCurso());
        holder.txtAnuncioFecha.setText(anuncioEntities.get(position).getPrecio());
    }

    @Override
    public int getItemCount() {
        return anuncioEntities.size();
    }

    public void setOnClickListener(View.OnClickListener listener){
        this.listener = listener;
    }

    @Override
    public void onClick(View v) {
        if(listener != null){
            listener.onClick(v);
        }
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{
        CardView crdContainerA;
        ImageView imgPerson;
        TextView txtAnuncioTitulo;
        TextView txtAnuncioLugar;
        TextView txtAnuncioMateria;
        TextView txtAnuncioFecha;

        public ViewHolder(View itemView) {
            super(itemView);
            crdContainerA = (CardView)itemView.findViewById(R.id.crdContainerA);
            txtAnuncioTitulo = (TextView)itemView.findViewById(R.id.txtAnuncioTitulo);
            txtAnuncioLugar = (TextView)itemView.findViewById(R.id.txtAnuncioLugar);
            txtAnuncioMateria = (TextView)itemView.findViewById(R.id.txtAnuncioMateria);
            txtAnuncioFecha = (TextView)itemView.findViewById(R.id.txtAnuncioFecha);
        }
    }
}
